package br.ucsal.bes20201.testequalidade.locadora;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import br.ucsal.bes20201.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;

import java.util.List;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		VeiculoBuilder builder = VeiculoBuilder.umVeiculo();

		List listaLocacao = Arrays.asList(
			builder.mas().comAnos(1).build(),
			builder.mas().comAnos(1).build(),
			builder.mas().comAnos(8).build(),
			builder.mas().comAnos(8).build(),
			builder.mas().comAnos(8).build()
		);

		Integer tempoLocacao = 3;

		Double valorLocacao = LocacaoBO.calcularValorTotalLocacao(listaLocacao, tempoLocacao);
		Double valorEsperado = 211.5d;

		assertEquals(valorEsperado, valorLocacao);
	}

}
