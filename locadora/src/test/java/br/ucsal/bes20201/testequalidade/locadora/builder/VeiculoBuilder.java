package br.ucsal.bes20201.testequalidade.locadora.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

import java.time.LocalDate;


public class VeiculoBuilder {

    public static final String PLACA_DEFAULT = "AAA-0000";
    public static final Modelo MODELO_DEFAULT = new Modelo("Popular Comum");
    public static final Integer ANO_DEFAULT = LocalDate.now().getYear() - 1;
    public static final Double DIARIA_DEFAULT = 15d;
    public static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;

    private String placa = PLACA_DEFAULT;
    private Modelo modelo = MODELO_DEFAULT;
    private Integer ano = ANO_DEFAULT;
    private Double diaria = DIARIA_DEFAULT;
    private SituacaoVeiculoEnum situacao = SITUACAO_DEFAULT;

    private VeiculoBuilder(){}

    public static VeiculoBuilder umVeiculo() {
        return new VeiculoBuilder();
    }

    public VeiculoBuilder comPlaca(String placa) {
        this.placa = placa;
        return this;
    }

    public VeiculoBuilder comModelo(Modelo modelo) {
        this.modelo = modelo;
        return this;
    }

    public VeiculoBuilder comAnoFabricacao(Integer ano) {
        this.ano = ano;
        return this;
    }

    public VeiculoBuilder comDiaria(Double diaria) {
        this.diaria = diaria;
        return this;
    }

    public VeiculoBuilder comAnos(Integer idade) {
        Integer anoAtual = LocalDate.now().getYear();
        return this.comAnoFabricacao(anoAtual - idade);
    }

    public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
        this.situacao = situacao;
        return this;
    }

    public VeiculoBuilder disponivel() {
        return this.comSituacao(SituacaoVeiculoEnum.DISPONIVEL);
    }

    public VeiculoBuilder locado() {
        return this.comSituacao(SituacaoVeiculoEnum.LOCADO);
    }

    public VeiculoBuilder emManutencao() {
        return this.comSituacao(SituacaoVeiculoEnum.MANUTENCAO);
    }

    public VeiculoBuilder mas() {
        return new VeiculoBuilder()
            .comAnoFabricacao(this.ano)
            .comPlaca(this.placa)
            .comDiaria(this.diaria)
            .comModelo(this.modelo)
            .comSituacao(this.situacao);
    }

    public Veiculo build() {
        Veiculo instance = new Veiculo();

        instance.setAno(this.ano);
        instance.setModelo(this.modelo);
        instance.setPlaca(this.placa);
        instance.setValorDiaria(this.diaria);
        instance.setSituacao(this.situacao);

        return instance;
    }


}
